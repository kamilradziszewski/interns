import Vue from "vue";
import Vuex from "vuex";

import axios from "axios";
import router from "@/router";

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    loading: false,
    error: "",
    users: [],
    modal: {
      isActive: false,
      content: "",
      id: null,
      redirect: false
    }
  },
  mutations: {
    addUsers(state, users) {
      state.users.unshift(...users);
    },
    removeUser(state, id) {
      const user = state.users.find(user => user.id === id);
      state.users.splice(state.users.indexOf(user), 1);
    },
    updateUser(state, updatedUser) {
      const user = state.users.find(
        user => user.id === updatedUser.id
      );
      state.users[state.users.indexOf(user)] = updatedUser;
      router.push("/");
    },
    openModal(state, { content, userId, redirect }) {
      state.modal = {
        isActive: true,
        content,
        userId,
        redirect
      };
    },
    closeModal(state) {
      state.modal = {
        isActive: false,
        content: "",
        id: null,
        redirect: false
      };
    }
  },
  actions: {
    fetchUsers({ commit }) {
      this.state.loading = true;
      this.state.error = "";

      axios
        .get("https://reqres.in/api/users?per_page=100")
        .then(res => {
          this.state.loading = false;

          commit("addUsers", res.data.data);
        })
        .catch(err => {
          this.state.loading = false;
          this.state.error = err;
        });
    },
    removeUser({ commit }, { userId, redirect }) {
      this.state.loading = true;
      this.state.error = "";

      axios
        .delete(`https://reqres.in/api/users/${userId}`)
        // eslint-disable-next-line no-unused-vars
        .then(res => {
          this.state.loading = false;

          commit("removeUser", userId);

          if (redirect) {
            router.push("/");
          }
        })
        .catch(err => {
          this.state.loading = false;
          this.state.error = err;
        });
    },
    addUser({ commit }, user) {
      this.state.loading = true;
      this.state.error = "";

      axios
        .post("https://reqres.in/api/users")
        .then(res => {
          this.state.loading = false;

          const newUser = {
            id: parseInt(res.data.id),
            first_name: user.first_name,
            last_name: user.last_name,
            avatar: user.avatar
          };
          commit("addUsers", [newUser]);
          router.push("/");
        })
        .catch(err => {
          this.state.loading = false;
          this.state.error = err;
        });
    }
  },
  getters: {
    filteredUsers(state) {
      return phrase =>
        state.users.filter(({ first_name, last_name }) => {
          const name = `${first_name} ${last_name}`;
          return name
            .toLowerCase()
            .includes(phrase.toString().toLowerCase());
        });
    },
    isLoading(state) {
      return state.loading;
    },
    isError(state) {
      return state.error;
    }
  },
  modules: {}
});
