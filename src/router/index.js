import Vue from "vue";
import VueRouter from "vue-router";
import Home from "../views/Home.vue";

Vue.use(VueRouter);

const routes = [
  {
    path: "/",
    name: "Home",
    component: Home
  },
  {
    path: "/add-user",
    name: "Add User",
    component: () =>
      import(
        /* webpackChunkName: "processUser" */ "../views/ProcessUser.vue"
      )
  },
  {
    path: "/edit-user",
    redirect: "/"
  },
  {
    path: "/edit-user/:id",
    name: "Edit User",
    component: () =>
      import(
        /* webpackChunkName: "processUser" */ "../views/ProcessUser.vue"
      )
  }
];

const router = new VueRouter({
  mode: "history",
  base: process.env.BASE_URL,
  routes
});

export default router;
